<?php

$config = array(
    'HTTP_SERVER'                  => 'http://qwerty.webninjashops.com',
    'HTTPS_SERVER'                 => 'https://qwerty.webninjashops.com',
    'HTTP_COOKIE_DOMAIN'           => 'qwerty.webninjashops.com',
    'HTTPS_COOKIE_DOMAIN'          => 'qwerty.webninjashops.com',

    'DIR_FS_CATALOG'               => '/var/www/sites/qwerty/',

    'DB_SERVER_USERNAME'           => 'qwerty',
    'DB_SERVER_PASSWORD'           => 'mz453u94',

    'CMS_BASE_DIRECTORY'           => '/var/www/cms/',
    'CORELIB_BASE_DIRECTORY'       => '/var/www/corelib/',
);

foreach ($config as $key => $value) {
    if (!defined($key)) {
        define($key, $value);
    }
}
